#include<bits/stdc++.h>
using namespace std;

class MyBigNumber {
private:
    string stn1;
    string stn2;
public:
    MyBigNumber(string num1, string num2) {
        this -> stn1 = stn1;
        this -> stn2 = stn2;
    }
    void setStn1(string stn1) {
        this -> stn1 = stn1;
    }
    string getStn1() {
        return stn1;
    }
    void setStn2(string stn2) {
        this -> stn2 = stn2;
    }
    string getStn2() {
        return stn2;
    }
    string sum(string stn1, string stn2) {
        string add = "";
        string res = "";
        int sum, n1, n2;
        int tmp = 0;
        int l1 = stn1.length();
        int l2 = stn2.length();
        int len = max(l1, l2);

        for (int i = 0; i <= len; ++i) {
            if (l1 - i - 1 < 0) n1 = 0;
            else n1 = stn1[l1 - i - 1] - '0';
            if (l2 - i - 1 < 0) n2 = 0;
            else n2 = stn2[l2 - i - 1] - '0';
            sum = n1 + n2 + tmp;
            tmp = sum/10;
            add = to_string(sum%10);
            res = add + res;
        } 
        
        return res;
    }
};
