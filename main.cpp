#include "Add2num.cpp"
#include <bits/stdc++.h>
using namespace std;

int main() {
    freopen("input.txt", "r", stdin);
    freopen("log.txt", "a", stdout);

    string stn1, stn2;
    cin >> stn1;
    cin >> stn2;
    MyBigNumber res(stn1, stn2);
    cout << "The first number is " << stn1 << endl;
    cout << "The second number is " << stn2 << endl;
    cout << "The result of the calculation is " << res.sum(stn1, stn2) << endl;
    auto time = chrono::system_clock::now();
    time_t end_time = chrono::system_clock::to_time_t(time);
 
    cout << "\nFinished computation at " << ctime(&end_time);
    cout << "------------------------------------------------" << endl;
    return 0;
}