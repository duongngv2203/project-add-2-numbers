using System;

namespace project_Add_2_Numbers
{
    public class MyBigNumber
    {
        private string Stn1 { get; set; }
        private string Stn2 { get; set; }

        public MyBigNumber() { }
        public MyBigNumber(string stn1, string stn2)
        {
            Stn1 = stn1;
            Stn2 = stn2;
        }

        public static string SumOf2Num(string stn1, string stn2)
        {
            string add = "";
            string res = "";
            int sum, n1, n2;
            int tmp = 0;
            int l1 = stn1.Length;
            int l2 = stn2.Length;
            int len = Math.Max(l1, l2);

            for (int i = 0; i < len; ++i)
            {
                if (l1 - i - 1 < 0) n1 = 0;
                else n1 = stn1[l1 - i - 1] - '0';
                if (l2 - i - 1 < 0) n2 = 0;
                else n2 = stn2[l2 - i - 1] - '0';
                sum = n1 + n2 + tmp;
                tmp = sum / 10;
                add = (sum % 10).ToString();
                res = add + res;
            }

            if (tmp != 0)
            {
                res = tmp.ToString() + res;
            }

            return res;
        }
        static void Main()
        {
            MyBigNumber myBigNumber = new MyBigNumber();
        }
    }
}
