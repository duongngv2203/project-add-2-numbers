# Project Add 2 numbers

    Add2Num là một project dùng để cộng 2 số lớn

## Install

    Download 2 file: Add2Num.cpp và main.cpp
    Sau đó tạo 2 file text có tựa: log.txt và input.txt

## How it work

    - Nhập dữ liệu là 2 chuỗi số vào file input.txt
    - Biên dịch và chạy file main.cpp
    - Kết quả sẽ được hiển thị trong file log.txt

## Technologies Used 

    C#, OOP
