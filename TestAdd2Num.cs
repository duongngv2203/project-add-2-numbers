using Xunit.Extensions;
using project_Add_2_Numbers;
using Assert = Xunit.Assert;

namespace Test_project_Add_2_numbers
{
    public class UnitTest_Add2Num
    {
        [Fact]
        public void Test1()
        {
            string result = MyBigNumber.SumOf2Num("123456789", "147896325");
            Assert.Equal("271353114", result);
        }
        [Fact]
        public void Test2()
        {
            string result = MyBigNumber.SumOf2Num("93014823", "12395223");
            Assert.Equal("105410046", result);
        }
        [Fact]
        public void Test3()
        {
            string result = MyBigNumber.SumOf2Num("13698745", "147896325");
            Assert.Equal("161595070", result);
        }
    }
}
